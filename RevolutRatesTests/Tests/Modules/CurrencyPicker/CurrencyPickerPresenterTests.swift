//
//  CurrencyPickerPresenterTests.swift
//  RevolutRatesTests
//
//  Created by Aleksandr Lykov on 01.11.2020.
//

import XCTest
@testable import RevolutRates

/// Tests for `CurrencyPicker` presenter.
final class CurrencyPickerPresenterTests: XCTestCase {
  private var presenter: CurrencyPickerPresenterProtocol?
  private var inputModel: RevolutRates.CurrencyPickerInputModel?

  override func setUp() {
    super.setUp()

    presenter = CurrencyPickerPresenter()

    let currencyPickerInputModel = RevolutRates.CurrencyPickerInputModel(
      type: .base,
      disabledPairs: [RevolutRates.CurrencyPair(base: .aud, quote: .bgn)].compactMap { $0 },
      callback: nil
    )
    inputModel = currencyPickerInputModel

    presenter?.configure(with: currencyPickerInputModel)
  }

  override func tearDown() {
    super.tearDown()

    presenter = nil
    inputModel = nil
  }

  func testCurrenciesFiltering() {
    guard let presenter = presenter else {
      return XCTFail("Presenter wasn't initialized")
    }

    guard let inputModel = inputModel else {
      return XCTFail("Input model wasn't initialized")
    }
    var availableCurrencies: Set<RevolutRates.Currency> = []
    switch inputModel.type {
    case .base:
      RevolutRates.CurrencyPair.allPairs.forEach { pair in
        guard inputModel.disabledPairs.allSatisfy({ $0 != pair }) else { return }
        availableCurrencies.insert(pair.base)
      }

    case let .quote(pickedCurrency):
      RevolutRates.CurrencyPair.allPairs.forEach { pair in
        guard pickedCurrency != pair.quote,
              pair.base == pickedCurrency,
              inputModel.disabledPairs.allSatisfy({ $0 != pair })
        else {
          return
        }
        availableCurrencies.insert(pair.quote)
      }
    }
    let currencies = RevolutRates.Currency.allCases.filter(availableCurrencies.contains)
    let itemsCount = presenter.numberOfItems(in: 0)

    XCTAssertEqual(itemsCount, currencies.count, "Wrong currencies filtering")

    (0..<itemsCount).forEach { row in
      XCTAssertEqual(
        presenter.item(at: IndexPath(row: row, section: 0)),
        currencies[row],
        "Wrong currencies filtering at row \(row)"
      )
    }
  }
}

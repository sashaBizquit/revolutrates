//
//  APIServiceManagerTests.swift
//  RevolutRatesTests
//
//  Created by Aleksandr Lykov on 01.11.2020.
//

import XCTest
@testable import RevolutRates

/// Tests for manager of client-service interactions.
final class APIServiceManagerTests: XCTestCase {
  private var manager: RequestPerforming?

  override func setUp() {
    super.setUp()

    manager = APIServiceManager()
  }

  override func tearDown() {
    super.tearDown()

    manager = nil
  }

  /// Test for the correct handling of an empty URL.
  func testEmptyURLRequest() {
    guard let manager = manager else {
      return XCTFail("Testable manager \(APIServiceManager.self) wasn't initialized")
    }
    let requestMock = URLRequestComponentsMock(baseURL: nil)

    manager.performRequest(urlRequestComponents: requestMock) { (result: Result<Int, APIServiceError>) in
      switch result {
      case .success:
        XCTFail("Empty URL wasn't handled at all")

      case let .failure(apiServiceError):
        switch apiServiceError {
        case let .requestProvidingFailed(requestProviderError):
          guard case .urlNotFound = requestProviderError else {
            return XCTFail("Empty URL wasn't handled before encoding step")
          }

        default:
          XCTFail("Empty URL wasn't handled on a providing step")
        }
      }
    }
  }
}

//
//  CurrencyRatesStorageManagerAssembly.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 31.10.2020.
//

import Foundation

enum CurrencyRatesStorageManagerAssembly: ManagerAssembly {
  static let manager: CurrencyRatesStorageManagerProtocol = CurrencyRatesStorageManager(storage: UserDefaults.standard)
}

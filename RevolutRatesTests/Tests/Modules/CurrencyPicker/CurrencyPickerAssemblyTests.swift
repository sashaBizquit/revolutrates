//
//  CurrencyPicker.swift
//  RevolutRatesTests
//
//  Created by Aleksandr Lykov on 01.11.2020.
//

import XCTest
@testable import RevolutRates

/// Tests for `CurrencyPicker` assembly.
final class CurrencyPickerAssemblyTests: XCTestCase {

  private var assembly: CurrencyPickerAssembly.Type?

  override func setUp() {
    super.setUp()

    assembly = CurrencyPickerAssembly.self
  }

  override func tearDown() {
    super.tearDown()

    assembly = nil
  }

  func testPresenterToViewConnection() {
    guard let assembly = assembly else {
      return XCTFail("Testable presenter wasn't initialized")
    }

    let view = assembly.view

    guard let presenter = view.presenter as? CurrencyPickerPresenter else {
      return XCTFail("Presenter type was changed")
    }

    XCTAssertNotNil(presenter.view, "No presenter-to-view connection")
  }
}

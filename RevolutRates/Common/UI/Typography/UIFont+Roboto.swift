//
//  UIFont+Roboto.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 28.10.2020.
//

import UIKit.UIFont

extension UIFont {

  /// Standard sized `Roboto-Regular` font.
  static var robotoRegular: UIFont {
    customFont(named: "Roboto-Regular")
  }

  /// Standard sized `Roboto-Medium` font.
  static var robotoMedium: UIFont {
    customFont(named: "Roboto-Medium")
  }

  // MARK: Private
  /// Returns a custom font, if available. Otherwise, it returns the system font.
  ///
  ///  - Parameter fontName: Name of a custom font.
  ///  - Returns: Returns a custom font, if available in the application. Otherwise, it returns the system font.
  private static func customFont(named fontName: String) -> UIFont {
    let size = UIFont.labelFontSize
    guard let customFont = UIFont(name: fontName, size: size) else {
      assertionFailure("Failed to load the \"\(fontName)\" font")
      return .systemFont(ofSize: size)
    }
    return customFont
  }
}


//
//  APIServiceManagerAssembly.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

enum APIServiceManagerAssembly: ManagerAssembly {
  static let manager: APIServiceManagerProtocol = APIServiceManager()
}

//
//  APIServiceManagerStub.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

/// A stub that provides a hard-wired response to requests required by a real object.
struct APIServiceManagerStub: APIServiceManagerProtocol {
  func currencyRates(with requestModel: CurrencyRatesRequestModel, completion: @escaping CurrencyRatesRequestCallback) {
    completion(.success([:]))
  }
}

//
//  CurrencyRatesStorageStub.swift
//  RevolutRatesTests
//
//  Created by Александр Лыков on 13.11.2020.
//

import Foundation

/// A stub that provides currncy rates storage logic.
final class CurrencyRatesStorageStub {

  // MARK: Properties

  private var storage: [String: Any]

  // MARK: Lifecycle

  init() {
    storage = [:]
  }
}

// MARK: - CurrencyRatesStorageProtocol

extension CurrencyRatesStorageStub: CurrencyRatesStorageProtocol {

  func data(forKey defaultName: String) -> Data? {
    return storage[defaultName] as? Data
  }

  func set(_ value: Any?, forKey defaultName: String) {
    storage[defaultName] = value
  }
}

//
//  ImageFormItem.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

/// UIImageView form item.
enum ImageFormItem {

  /// Images located in the Assets.xcassets directory.
  case iconKey(ImageAssetKey)

  /// No image.
  case empty
}

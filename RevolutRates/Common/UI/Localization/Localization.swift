//
//  Localization.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

/// Localization namespace.
enum Localization {

  /// System alert localizations.
  enum Alert: LocalizationProvider {
	  case error
	  case ok
    case delete
    case cancel
  }

  /// Currencies localization.
  enum Currency: LocalizationProvider {
    case aud
    case bgn
    case brl
    case cad
    case chf
    case cny
    case czk
    case dkk
    case eur
    case gbp
    case hkd
    case hrk
    case huf
    case idr
    case ils
    case inr
    case isk
    case jpy
    case krw
    case mxn
    case myr
    case nok
    case nzd
    case php
    case pln
    case ron
    case rub
    case sek
    case sgd
    case thb
    case usd
    case zar
  }

  /// RateList module localizations.
  enum RatesList: LocalizationProvider {
    case title
    case addRate
    case repeatLoading
    case choosePair
    case removeRate
  }

  /// CurrencyPicker module localizations.
  enum CurrencyPicker: LocalizationProvider {
    case baseTitle
    case quoteTitle
  }
}

//
//  CurrencyServiceProtocol.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

typealias CurrencyRatesRequestModel = [CurrencyPair]
typealias CurrencyRatesResponseModel = [String: Double]
typealias CurrencyRatesRequestCallback = RequestPerformingCallback<CurrencyRatesResponseModel>

/// Protocol for interaction with the part of the server responsible for currencies.
protocol CurrencyServiceProtocol {

  /// Request to update rates ​​of currency pairs.
  ///
  /// - Parameter requestModel: Request model representing an array of currency pairs.
  /// - Parameter completion: The block to execute after getting response.
  func currencyRates(with requestModel: CurrencyRatesRequestModel, completion: @escaping CurrencyRatesRequestCallback)
}

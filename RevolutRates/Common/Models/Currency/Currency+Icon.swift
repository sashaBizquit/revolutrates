//
//  Currency+Icon.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 25.10.2020.
//

extension Currency {
  var icon: ImageAssetKey {
    switch self {
    case .aud:
      return .aud

    case .bgn:
      return .bgn

    case .brl:
      return .brl

    case .cad:
      return .cad

    case .chf:
      return .chf

    case .cny:
      return .cny

    case .czk:
      return .czk

    case .dkk:
      return .dkk

    case .eur:
      return .eur

    case .gbp:
      return .gbp

    case .hkd:
      return .hkd

    case .hrk:
      return .hrk

    case .huf:
      return .huf

    case .idr:
      return .idr

    case .ils:
      return .ils

    case .inr:
      return .inr

    case .isk:
      return .isk

    case .jpy:
      return .jpy

    case .krw:
      return .krw

    case .mxn:
      return .mxn

    case .myr:
      return .myr

    case .nok:
      return .nok

    case .nzd:
      return .nzd

    case .php:
      return .php

    case .pln:
      return .pln

    case .ron:
      return .ron

    case .rub:
      return .rub

    case .sek:
      return .sek

    case .sgd:
      return .sgd

    case .thb:
      return .thb

    case .usd:
      return .usd

    case .zar:
      return .zar
    }
  }
}

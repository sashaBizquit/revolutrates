//
//  CurrencyPickerAssembly.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 29.10.2020.
//

enum CurrencyPickerAssembly: ModuleAssembly {
  static var view: CurrencyPickerViewController {
    let presenterInstance = presenter
    let view = CurrencyPickerViewController(presenter: presenterInstance)
    presenterInstance.view = view
    return view
  }

  static var presenter: CurrencyPickerPresenter {
    CurrencyPickerPresenter()
  }
}


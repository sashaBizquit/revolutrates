//
//  CurrencyTableViewCellFormItem.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 30.10.2020.
//

/// Default `CurrencyTableViewCell` decoration instance.
struct CurrencyTableViewCellFormItem: CurrencyTableViewCellFormItemProtocol {
  let iconItem: ImageFormItem
  let id: String
  let name: String
}

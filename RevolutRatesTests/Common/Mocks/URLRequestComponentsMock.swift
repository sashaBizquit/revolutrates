//
//  URLRequestComponentsMock.swift
//  RevolutRatesTests
//
//  Created by Aleksandr Lykov on 01.11.2020.
//

import Foundation

struct URLRequestComponentsMock: URLRequestComponents {
  private(set) var baseURL: URL?
  private(set) var method: HTTPMethod = .get
  private(set) var path: String = ""
  private(set) var parameters: Parameters?
}

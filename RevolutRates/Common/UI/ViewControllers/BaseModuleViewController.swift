//
//  ModuleViewController.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

import UIKit

/// Base `UIViewConttoller` that represents the View layer of the module architecture.
class ModuleViewController<Presenter>: UIViewController {

  /// Presenter instance.
  let presenter: Presenter

  // MARK: Lifecycle
  
  required init(presenter: Presenter) {
    self.presenter = presenter
    
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    assertionFailure("init(coder:) has not been implemented")
    return nil
  }
}

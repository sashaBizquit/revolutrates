//
//  UIViewController+ModuleView.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 26.10.2020.
//

import UIKit

extension UIViewController: ModuleView {

  func showModule<T: ModuleView>(
    providedBy factory: ModuleFactory<T>,
    type: TransitionType,
    animated isAnimated: Bool,
    configure: Callback<T>?
  ) {
    let destination = factory()
    configure?(destination)

    guard let destinationViewController = destination as? UIViewController else { return assertionFailure() }

    switch type {
    case let .present(type):
      let destination: UIViewController
      switch type {
      case .default:
        destination = destinationViewController

      case .inNavigationController:
        destination = UINavigationController(rootViewController: destinationViewController)
      }

      present(destination, animated: isAnimated, completion: nil)

    case .push:
      guard let navigationController = navigationController else { return assertionFailure() }
      navigationController.pushViewController(destinationViewController, animated: isAnimated)
    }
  }

  func close(animated isAnimated: Bool, completion: VoidCallback?) {
    if let navigationController = navigationController {
      // If we want to close a controller that is inside the UINavigationController, there are two different scenarios.
      if let index = navigationController.viewControllers.firstIndex(of: self), index > 0 {
        // First scenario: if current UIViewController that is being closed is not the root one.
        // Then the view of controllers up to the one preceding the current one pops up.
        let previousViewController = navigationController.viewControllers[index - 1]

        navigationController.popToViewController(previousViewController, animated: isAnimated)

        // popToViewController(:, animated:) interface doesn't provide completion
        completion?()
      } else {
        // Otherwise dismiss navigation controller itself.
        navigationController.dismiss(animated: isAnimated, completion: completion)
      }
    } else if presentingViewController != nil {
      /// If current view controller is being presented then dismiss it.
      dismiss(animated: isAnimated, completion: completion)
    } else {
      assertionFailure("No close action found for \(self)")
    }
  }
}

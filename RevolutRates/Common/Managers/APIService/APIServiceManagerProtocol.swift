//
//  APIServiceManagerProtocol.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

/// Typealias of all supported services.
typealias APIServiceManagerProtocol = CurrencyServiceProtocol

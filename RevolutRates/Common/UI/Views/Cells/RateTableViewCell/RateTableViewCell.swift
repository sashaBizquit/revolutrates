//
//  RateTableViewCell.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 28.10.2020.
//

import UIKit

/// The visual representation of a currency rate with exchange values in a table view.
final class RateTableViewCell: UITableViewCell {

  /// View constnats.
  private enum Constants {
    /// The font used in the cell headers.
    static let mainFont = UIFont.robotoMedium.withSize(20)

    /// The font used in the subtitles of the cell.
    static let supportiveFont = UIFont.robotoRegular.withSize(14)
  }

  /// Сontainer with a padding for cell content.
  private let containerView = UIView()

  /// Leading title label.
  private let leadingTitle = UILabel()

  /// Leading subtitle label.
  private let leadingSubtitle = UILabel()

  /// Trailing title label.
  private let trailingTitle = UILabel()

  /// Trailing subtitle label.
  private let trailingSubtitle = UILabel()

  // MARK: Lifecycle

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)

    setupContentView()
  }

  required init?(coder: NSCoder) {
    assertionFailure("init(coder:) has not been implemented")
    return nil
  }

  // MARK: Private

  private func setupContentView() {
    setupContainerView()
    setupLeadingTitle()
    setupLeadingSubtitle()
    setupTrailingTitle()
    setupTrailingSubtitle()
  }

  private func setupContainerView() {
    contentView.addSubview(containerView)

    containerView.translatesAutoresizingMaskIntoConstraints = false

    NSLayoutConstraint.activate(
      [
        containerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
        containerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
        containerView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
        containerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -20),
      ]
    )
  }

  private func setupLeadingTitle() {
    containerView.addSubview(leadingTitle)

    leadingTitle.translatesAutoresizingMaskIntoConstraints = false
    leadingTitle.font = Constants.mainFont
    leadingTitle.textColor = Palette.Gray.dark
    leadingTitle.textAlignment = .natural

    NSLayoutConstraint.activate(
      [
        leadingTitle.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
        leadingTitle.topAnchor.constraint(equalTo: containerView.topAnchor)
      ]
    )
  }

  private func setupLeadingSubtitle() {
    containerView.addSubview(leadingSubtitle)

    leadingSubtitle.translatesAutoresizingMaskIntoConstraints = false
    leadingSubtitle.font = Constants.supportiveFont
    leadingSubtitle.textColor = Palette.Gray.medium
    leadingSubtitle.textAlignment = .natural

    NSLayoutConstraint.activate(
      [
        leadingSubtitle.topAnchor.constraint(equalTo: leadingTitle.bottomAnchor),
        leadingSubtitle.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
        leadingSubtitle.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
      ]
    )
  }

  private func setupTrailingTitle() {
    containerView.addSubview(trailingTitle)

    trailingTitle.translatesAutoresizingMaskIntoConstraints = false
    trailingTitle.font = Constants.mainFont
    trailingTitle.textColor = Palette.Gray.dark
    trailingTitle.textAlignment = .naturalInverse
    trailingTitle.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)

    NSLayoutConstraint.activate(
      [
        trailingTitle.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
        trailingTitle.topAnchor.constraint(equalTo: containerView.topAnchor),
        trailingTitle.leadingAnchor.constraint(greaterThanOrEqualTo: leadingTitle.trailingAnchor)
      ]
    )
  }

  private func setupTrailingSubtitle() {
    containerView.addSubview(trailingSubtitle)

    trailingSubtitle.translatesAutoresizingMaskIntoConstraints = false
    trailingSubtitle.font = Constants.supportiveFont
    trailingSubtitle.textColor = Palette.Gray.medium
    trailingSubtitle.textAlignment = .naturalInverse
    trailingTitle.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)

    NSLayoutConstraint.activate(
      [
        trailingSubtitle.topAnchor.constraint(equalTo: trailingTitle.bottomAnchor),
        trailingSubtitle.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
        trailingSubtitle.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
        trailingSubtitle.leadingAnchor.constraint(greaterThanOrEqualTo: leadingSubtitle.trailingAnchor)
      ]
    )
  }
}

// MARK: - FormItemConfigurable

extension RateTableViewCell: FormItemConfigurable {
  func configure(with formItem: RateTableViewCellFormItemProtocol) {
    leadingTitle.text = formItem.leadingTitle
    leadingSubtitle.text = formItem.leadingSubtitle
    trailingSubtitle.text = formItem.trailingSubtitle

    let prefixAttributes: [NSAttributedString.Key: Any] = [.font: Constants.mainFont]
    let attributedTrailingTitle = NSMutableAttributedString(string: formItem.trailingTitlePrefix, attributes: prefixAttributes)

    if let trailingTitleSuffix = formItem.trailingTitleSuffix, !trailingTitleSuffix.isEmpty {
      let suffixAttributes: [NSAttributedString.Key: Any] = [.font: Constants.supportiveFont]
      let attributedSuffix = NSAttributedString(string: trailingTitleSuffix, attributes: suffixAttributes)
      attributedTrailingTitle.append(attributedSuffix)
    }

    trailingTitle.attributedText = attributedTrailingTitle
  }
}

//
//  RatesListPresenterTests.swift
//  RevolutRatesTests
//
//  Created by Aleksandr Lykov on 01.11.2020.
//

import XCTest
@testable import RevolutRates

/// Tests for `RatesList` presenter.
final class RatesListPresenterTests: XCTestCase {
  private var presenter: RatesListPresenterProtocol?
  private var rates: [RevolutRates.CurrencyRate] = []

  override func setUp() {
    super.setUp()

    rates = [
      RevolutRates.CurrencyPair(base: .aud, quote: .bgn)
    ].compactMap { pair in
      guard let pair = pair else { return nil }
      return RevolutRates.CurrencyRate(pair: pair, rate: nil)
    }
    let storageManager = CurrencyRatesStorageManagerStub(currencyRates: rates)

    presenter = RatesListPresenter(
      apiService: APIServiceManagerStub(),
      currencyRatesStorageManager: storageManager
    )
  }

  override func tearDown() {
    super.tearDown()

    presenter = nil
  }

  func testReloadData() {
    guard let presenter = presenter else {
      return XCTFail("Testable presenter wasn't initialized")
    }

    XCTAssertEqual(presenter.numberOfSections, 0, "Items reloaded")

    presenter.reloadData()

    XCTAssertEqual(presenter.numberOfSections, 1, "More than 1 table section was created")

    let itemsCount = presenter.numberOfItems(in: 0) - 1

    XCTAssertEqual(itemsCount, rates.count, "")

    (0...itemsCount).forEach { itemIndex in
      guard let item = presenter.item(at: IndexPath(row: itemIndex, section: 0)) else {
        return XCTFail("Index \(itemIndex) out of range")
      }
      switch item {
      case .addRate:
        XCTAssertEqual(itemIndex, 0, "Add rate at wrong row")

      case let .rate(rate):
        XCTAssertEqual(rate, rates[safe: itemIndex - 1], "Add rate at wrong row")
      }
    }
  }
}

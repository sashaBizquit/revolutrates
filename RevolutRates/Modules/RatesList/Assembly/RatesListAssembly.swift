//
//  RatesListAssembly.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

enum RatesListAssembly: ModuleAssembly {
  static var view: RatesListViewController {
    let presenterInstance = presenter
	  let view = RatesListViewController(presenter: presenterInstance)
    presenterInstance.view = view
    return view
  }

  static var presenter: RatesListPresenter {
    RatesListPresenter(
      apiService: APIServiceManagerAssembly.manager,
      currencyRatesStorageManager: CurrencyRatesStorageManagerAssembly.manager
    )
  }
}

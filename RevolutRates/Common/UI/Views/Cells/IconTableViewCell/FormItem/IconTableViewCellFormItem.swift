//
//  IconTableViewCellFormItem.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 28.10.2020.
//

/// Default `IconTableViewCell` decoration instance.
struct IconTableViewCellFormItem: IconTableViewCellFormItemProtocol {
  let iconItem: ImageFormItem
  let title: String?
}

//
//  FormItemConfigurable.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

/// Protocol of the instance to be decorated.
protocol FormItemConfigurable {
  associatedtype FormItem

  /// Decorating an object.
  ///
  /// - Parameter formItem: Decorating object.
  func configure(with formItem: FormItem)
}

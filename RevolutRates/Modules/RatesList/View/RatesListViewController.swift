//
//  RatesListViewController.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

import UIKit

final class RatesListViewController: ModuleViewController<RatesListPresenterProtocol> {

  // MARK: UI components

  private lazy var tableView = UITableView()
  private lazy var addRateView = AddRateView()

  // MARK: Lifecycle

  override func loadView() {
    setupView()
    setupTableView()
    setupAddRateView()
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    configureUI()
    presenter.reloadData()
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    presenter.viewDidAppear()
  }

  // MARK: Private

  private func setupView() {
    view = UIView()
  }

  private func setupTableView() {
    view.addSubview(tableView)

    tableView.translatesAutoresizingMaskIntoConstraints = false

    NSLayoutConstraint.activate(
      [
        tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
        tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
      ]
    )
  }

  private func setupAddRateView() {
    view.addSubview(addRateView)

    addRateView.translatesAutoresizingMaskIntoConstraints = false

    NSLayoutConstraint.activate(
      [
        addRateView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
        addRateView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
        addRateView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
        addRateView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
      ]
    )
  }

  private func configureUI() {
    configureView()
    configureNavigationItem()
    configureTableView()
    configureAddRateView()
  }

  private func configureView() {
    view.backgroundColor = Palette.white
  }

  private func configureNavigationItem() {
    navigationItem.title = Localization.RatesList.title.localized
  }

  private func configureTableView() {
    tableView.dataSource = self
    tableView.delegate = self
    tableView.separatorStyle = .none
    tableView.sectionHeaderHeight = .leastNormalMagnitude
    tableView.sectionFooterHeight = .leastNormalMagnitude
    tableView.registerReusable(RateTableViewCell.self)
    tableView.registerReusable(IconTableViewCell.self)
  }

  private func configureAddRateView() {
    addRateView.delegate = self
    addRateView.isHidden = true

    let localization = Localization.RatesList.self
    let formItem = AddRateViewFormItem(
      topImageViewItem: .iconKey(.plus),
      middleButtonItem: ButtonFormItem(title: localization.addRate.localized),
      caption: localization.choosePair.localized
    )
    addRateView.configure(with: formItem)
  }
}

// MARK: - RatesListViewProtocol

extension RatesListViewController: RatesListViewProtocol {
  func updateView(placeholderHidden isPlaceholderHidden: Bool) {
    DispatchQueue.main.async {
      self.addRateView.isHidden = isPlaceholderHidden
      self.tableView.reloadData()
    }
  }
}

// MARK: - UITableViewDataSource

extension RatesListViewController: UITableViewDataSource {

  func numberOfSections(in tableView: UITableView) -> Int {
    return presenter.numberOfSections
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return presenter.numberOfItems(in: section)
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let item = presenter.item(at: indexPath) else {
      assertionFailure("No item for index path \(indexPath)")
      return UITableViewCell()
    }

    switch item {
    case let .rate(currencyRate):
      guard let cell = tableView.dequeueReusableCell(withReusable: RateTableViewCell.self, for: indexPath) else {
        assertionFailure("No cell registered with type \(RateTableViewCell.self)")
        return UITableViewCell()
      }

      let pair = currencyRate.pair
      var prefix: String = "-"
      var suffix: String?
      if let rateValue = currencyRate.rate,
         let rate = NumberFormatterHelper.rateString(from: rateValue),
         let lastIndex = rate.indices.last
      {
        let delimerIndex = rate.index(lastIndex, offsetBy: -1)
        prefix = String(rate.prefix(upTo: delimerIndex))
        suffix = String(rate.suffix(from: delimerIndex))
      }
      let formItem = RateTableViewCellFormItem(
        leadingTitle: "1 " + pair.base.id,
        leadingSubtitle: pair.base.name,
        trailingTitlePrefix: prefix,
        trailingTitleSuffix: suffix,
        trailingSubtitle: pair.quote.name
      )
      cell.configure(with: formItem)
      return cell

    case .addRate:
      guard let cell = tableView.dequeueReusableCell(withReusable: IconTableViewCell.self, for: indexPath) else {
        assertionFailure("No cell registered with type \(RateTableViewCell.self)")
        return UITableViewCell()
      }

      let formItem = IconTableViewCellFormItem(iconItem: .iconKey(.plus), title: Localization.RatesList.addRate.localized)
      cell.configure(with: formItem)
      return cell
    }
  }
}

// MARK: - UITableViewDelegate

extension RatesListViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    presenter.didSelectItem(at: indexPath)
  }
}

// MARK: - AddRateViewDelegate

extension RatesListViewController: AddRateViewDelegate {

  func addRateViewDidTapMiddleButton(_ addRateView: AddRateView) {
    presenter.didTapAddRate()
  }
}

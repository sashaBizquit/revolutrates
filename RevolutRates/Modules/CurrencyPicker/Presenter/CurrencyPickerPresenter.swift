//
//  CurrencyPickerPresenter.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 29.10.2020.
//

import Foundation

final class CurrencyPickerPresenter {

  // MARK: Internal properties

  weak var view: CurrencyPickerViewProtocol?

  // MARK: Private properties

  private var inputModel: CurrencyPickerInputModel? {
    didSet {
      updateCurrencies()
    }
  }
  private var currencies: [Currency] = Currency.allCases

  private var type: CurrencyPickerType {
    inputModel?.type ?? .base
  }

  private func updateCurrencies() {
    guard let inputModel = inputModel else { return }

    var availableCurrencies: Set<Currency> = []
    switch inputModel.type {
    case .base:
      CurrencyPair.allPairs.forEach { pair in
        guard inputModel.disabledPairs.allSatisfy({ $0 != pair }) else { return }
        availableCurrencies.insert(pair.base)
      }

    case let .quote(pickedCurrency):
      CurrencyPair.allPairs.forEach { pair in
        guard pickedCurrency != pair.quote,
              pair.base == pickedCurrency,
              inputModel.disabledPairs.allSatisfy({ $0 != pair })
        else {
          return
        }
        availableCurrencies.insert(pair.quote)
      }
    }
    currencies = Currency.allCases.filter(availableCurrencies.contains)
  }

  private func showQuoteCurrencyPicker(with pickedCurrency: Currency) {
    let quoteCallback: Callback<CurrencyPair> = { [weak self] pair in
      guard let self = self else { return }
      self.inputModel?.callback?(pair)
      self.view?.close()
    }
    let quoteInputModel = CurrencyPickerInputModel(
      type: .quote(base: pickedCurrency),
      disabledPairs: inputModel?.disabledPairs ?? [],
      callback: quoteCallback
    )
    view?.showCurrnecyPicker(with: quoteInputModel)
  }
}

extension CurrencyPickerPresenter: CurrencyPickerPresenterProtocol {

  var navigationTitle: String? {
    let localization = Localization.CurrencyPicker.self
    switch type {
    case .base:
      return localization.baseTitle.localized

    case .quote:
      return localization.quoteTitle.localized
    }
  }

  var numberOfSections: Int {
    currencies.isEmpty ? 0 : 1
  }

  func numberOfItems(in section: Int) -> Int {
    return currencies.count
  }

  func item(at indexPath: IndexPath) -> Currency? {
    return currencies[safe: indexPath.row]
  }

  func didSelectItem(at indexPath: IndexPath) {
    guard let pickedCurrency = item(at: indexPath) else { return }

    switch type {
    case .base:
      showQuoteCurrencyPicker(with: pickedCurrency)

    case let .quote(base):
      guard let currencyPair = CurrencyPair(base: base, quote: pickedCurrency) else { return assertionFailure() }
      inputModel?.callback?(currencyPair)
    }
  }

  func configure(with inputModel: CurrencyPickerInputModel) {
    self.inputModel = inputModel
  }
}

//
//  CurrencyRatesStorageManagerProtocol.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 31.10.2020.
//

/// Protocol for storing and gaining access to currency rates.
protocol CurrencyRatesStorageManagerProtocol {

  /// All currency rates stored in the application.
  var currencyRates: [CurrencyRate] { get }

  /// Adds a new currency rate to storage.
  ///
  /// - Parameter currencyRate: New currency rate.
  func addRate(_ currencyRate: CurrencyRate)

  /// Updates the currency rates stored in the storage without adding new one.
  ///
  /// - Parameter updatedRates: Updated currency rates.
  func updateRates(with updatedRates: [CurrencyRate])

  /// Removes a currency rate from storage.
  ///
  /// - Parameter currencyRate: The currency pair to be removed.
  func removeRate(_ currencyRate: CurrencyRate)

  /// Removes all currency rates from storage.
  func removeAllRates()
}

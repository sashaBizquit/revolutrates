//
//  Palette.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

import UIKit

/// Application color palette.
enum Palette {

  /// Blue colors group.
  enum Blue {
    /// RGB: 0, 117, 235
	  static let intense = UIColor(withRed: 0, green: 117, blue: 235)
  }

  /// Gray colors group.
  enum Gray {
    /// RGB: 25, 28, 31
	  static let dark = UIColor(withRed: 25, green: 28, blue: 31)

    /// RGB: 139, 149, 158
    static let medium = UIColor(withRed: 139, green: 149, blue: 158)
  }

  /// Transparent color object.
  static let clear: UIColor = .clear

  /// RGB: 255, 255, 255
  static let white: UIColor = .white

  /// RGB: 0, 0, 20
  static let black = UIColor(withRed: 0, green: 0, blue: 20)
}

extension UIColor {
  convenience init(withRed red: UInt8, green: UInt8, blue: UInt8, alpha: CGFloat = 1) {
	  self.init(red: CGFloat(red)/255, green: CGFloat(green)/255, blue: CGFloat(blue)/255, alpha: alpha)
  }
}

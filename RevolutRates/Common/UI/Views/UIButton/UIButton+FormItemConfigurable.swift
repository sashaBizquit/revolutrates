//
//  UIButton+FormItemConfigurable.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 01.11.2020.
//

import UIKit

extension UIButton: FormItemConfigurable {
  func configure(with formItem: ButtonFormItem) {
    setTitle(formItem.title, for: .normal)

    if let imageName = formItem.imageKey?.rawValue {
      if let image = UIImage(named: imageName) {
        setImage(image, for: .normal)
      } else {
        assertionFailure("No image found: \(imageName)")
      }
    }
  }
}

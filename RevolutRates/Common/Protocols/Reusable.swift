//
//  Reusable.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

import UIKit

/// Protocol of the reused object.
protocol Reusable: AnyObject {

  /// The identifier used in the process of reusing the object.
  static var reuseIdentifier: String { get }
}

extension Reusable {
  static var reuseIdentifier: String {
	  "\(self)"
  }
}

extension UITableViewCell: Reusable {}

extension UITableView {

  /// Registers a reusable objects for use in creating new table cells.
  ///
  /// - Parameter reusable: Type of a reusable object.
  func registerReusable<T: Reusable>(_ reusable: T.Type) {
	  register(T.self, forCellReuseIdentifier: reusable.reuseIdentifier)
  }

  /// Returns a reusable table-view cell object for the specified cell class type if it was registered and adds it to the table.
  ///
  /// - Parameter reusable: Type of a reusable object.
  /// - Parameter indexPath: The index path specifying the location of the cell.
  func dequeueReusableCell<T: UITableViewCell>(withReusable reusable: T.Type, for indexPath: IndexPath) -> T? {
	  return dequeueReusableCell(withIdentifier: reusable.reuseIdentifier, for: indexPath) as? T
  }
}

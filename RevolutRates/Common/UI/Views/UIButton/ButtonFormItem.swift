//
//  ButtonFormItem.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 01.11.2020.
//

/// UIButton form item.
struct ButtonFormItem {

  /// Button title text.
  private(set) var title: String?

  /// Button image key.
  private(set) var imageKey: ImageAssetKey?
}

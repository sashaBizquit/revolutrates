//
//  TablePresenting.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 24.10.2020.
//

import Foundation

/// Protocol for a presenter that provides data for displaying a table view.
protocol TablePresenting {

  /// Number of sections in the table view.
  var numberOfSections: Int { get }

  /// Returns number of rows in a given section of a table view.
  ///
  /// - Parameter section: An index number identifying a section in tableView.
  func numberOfItems(in section: Int) -> Int

  /// Tells the presenter a row is selected.
  func didSelectItem(at indexPath: IndexPath)
}

extension TablePresenting {
  var numberOfSections: Int { 1 }

  func didSelectItem(at indexPath: IndexPath) {}
}

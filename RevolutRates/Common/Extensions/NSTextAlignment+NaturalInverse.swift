//
//  NSTextAlignment+NaturalInverse.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 28.10.2020.
//

import UIKit

extension NSTextAlignment {

  /// Returns the opposite alignment associated with the current app localization.
  static var naturalInverse: NSTextAlignment {
    UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .left : .right
  }
}

//
//  CommonConstants.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 24.10.2020.
//

enum CommonConstants {
  /// URL to which all requests will be executed.
  static let baseURL = "https://europe-west1-revolut-230009.cloudfunctions.net/revolut-ios"

  /// Сurrency key length.
  static let currencyIdLength = 3
}

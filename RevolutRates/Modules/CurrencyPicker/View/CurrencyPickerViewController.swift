//
//  CurrencyPickerViewController.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 29.10.2020.
//

import UIKit

final class CurrencyPickerViewController: ModuleViewController<CurrencyPickerPresenterProtocol>, CurrencyPickerViewProtocol {

  // MARK: UI components

  private lazy var tableView = UITableView()

  // MARK: Lifecycle

  override func loadView() {
    setupView()
    setupTableView()
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    configureUI()
  }

  // MARK: Private

  private func setupView() {
    view = UIView()
  }

  private func setupTableView() {
    view.addSubview(tableView)

    tableView.translatesAutoresizingMaskIntoConstraints = false

    NSLayoutConstraint.activate(
      [
        tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
        tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
      ]
    )
  }

  private func configureUI() {
    configureView()
    configureNavigationItem()
    configureTableView()
  }

  private func configureView() {
    view.backgroundColor = Palette.white
  }

  private func configureNavigationItem() {
    navigationItem.title = presenter.navigationTitle
  }

  private func configureTableView() {
    tableView.dataSource = self
    tableView.delegate = self
    tableView.separatorStyle = .none
    tableView.sectionHeaderHeight = .leastNormalMagnitude
    tableView.sectionFooterHeight = .leastNormalMagnitude
    tableView.registerReusable(CurrencyTableViewCell.self)
  }
}

// MARK: - UITableViewDataSource

extension CurrencyPickerViewController: UITableViewDataSource {

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return presenter.numberOfItems(in: section)
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let item = presenter.item(at: indexPath),
          let cell = tableView.dequeueReusableCell(withReusable: CurrencyTableViewCell.self, for: indexPath)
    else {
      assertionFailure()
      return UITableViewCell()
    }

    let formItem = CurrencyTableViewCellFormItem(iconItem: .iconKey(item.icon), id: item.id, name: item.name)
    cell.configure(with: formItem)

    return cell
  }
}

// MARK: - UITableViewDelegate

extension CurrencyPickerViewController: UITableViewDelegate {

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    presenter.didSelectItem(at: indexPath)
  }
}

// MARK: - CurrencyPickerInputProtocol

extension CurrencyPickerViewController: CurrencyPickerInputProtocol {

  func configure(with inputModel: CurrencyPickerInputModel) {
    presenter.configure(with: inputModel)
  }
}

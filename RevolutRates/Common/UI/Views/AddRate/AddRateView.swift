//
//  AddRateView.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 29.10.2020.
//

import UIKit

/// Methods for managing taps and performing other actions in a `AddRateView`.
protocol AddRateViewDelegate: AnyObject {

  /// Called after the user taps middle button.
  func addRateViewDidTapMiddleButton(_ addRateView: AddRateView)
}

final class AddRateView: UIView {

  // MARK: Internal properties

  weak var delegate: AddRateViewDelegate?

  // MARK: Private properties

  /// `UIStackView` containing all of AddRateView's UI elements.
  private let contentStackView = UIStackView()

  /// Top image.
  private let topImageView = UIImageView()

  /// Middle "text" button.
  private let middleButton = UIButton()

  /// Caption label.
  private let captionLabel = UILabel()

  // MARK: Lifecycle

  override init(frame: CGRect) {
    super.init(frame: frame)
    setupUI()
  }

  required init?(coder: NSCoder) {
    assertionFailure("init(coder:) has not been implemented")
    return nil
  }

  // MARK: Private methods

  private func setupUI() {
    backgroundColor = Palette.white

    setupContentStackView()
    setupTopImageView()
    setupMiddleButton()
    setupCaptionLabel()
  }

  private func setupContentStackView() {
    addSubview(contentStackView)

    contentStackView.translatesAutoresizingMaskIntoConstraints = false
    contentStackView.spacing = 16
    contentStackView.axis = .vertical
    contentStackView.alignment = .center

    NSLayoutConstraint.activate(
      [
        contentStackView.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor),
        contentStackView.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
        contentStackView.leadingAnchor.constraint(greaterThanOrEqualTo: safeAreaLayoutGuide.leadingAnchor, constant: 32),
        contentStackView.topAnchor.constraint(greaterThanOrEqualTo: safeAreaLayoutGuide.topAnchor, constant: 16)
      ]
    )
  }

  private func setupTopImageView() {
    contentStackView.addArrangedSubview(topImageView)

    topImageView.translatesAutoresizingMaskIntoConstraints = false

    NSLayoutConstraint.activate(
      [
        topImageView.heightAnchor.constraint(equalToConstant: 80),
        topImageView.widthAnchor.constraint(equalToConstant: 80)
      ]
    )
  }

  private func setupMiddleButton() {
    contentStackView.addArrangedSubview(middleButton)

    middleButton.setTitleColor(Palette.Blue.intense, for: .normal)
    middleButton.titleLabel?.font = UIFont.robotoMedium.withSize(20)
    middleButton.addTarget(self, action: #selector(didTapMiddleButton), for: .touchUpInside)
  }

  private func setupCaptionLabel() {
    contentStackView.addArrangedSubview(captionLabel)

    captionLabel.font = UIFont.robotoRegular.withSize(16)
    captionLabel.textColor = Palette.Gray.medium
    captionLabel.numberOfLines = 0
    captionLabel.textAlignment = .center
  }

  @objc private func didTapMiddleButton() {
    delegate?.addRateViewDidTapMiddleButton(self)
  }
}

// MARK: - FormItemConfigurable

extension AddRateView: FormItemConfigurable {
  func configure(with formItem: AddRateViewFormItemProtocol) {
    topImageView.configure(with: formItem.topImageViewItem)
    middleButton.configure(with: formItem.middleButtonItem)
    captionLabel.text = formItem.caption
  }
}

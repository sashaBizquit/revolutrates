//
//  RatesListPresenter.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

import Foundation

final class RatesListPresenter {

  // MARK: Internal properties
  /// Instance providing access to the `View` architectural layer covered by protocol.
  weak var view: RatesListViewProtocol?

  // MARK: Private properties
  /// Client-server communication manager.
  private let apiService: CurrencyServiceProtocol

  /// Manager of storing and gaining access to currency rates.
  private let currencyRatesStorageManager: CurrencyRatesStorageManagerProtocol

  /// Timer for retrying currency rates update request.
  private var reloadTimer: Timer?

  /// Table items.
  @Atomic private var items: [RatesListItem] = []

  /// The number of requests currently in progress.
  @Atomic private var currentRequestsCount = 0

  /// Displaying currency rates.
  private var rates: [CurrencyRate] {
    currencyRatesStorageManager.currencyRates
  }

  /// Displaying currency pairs.
  private var currencyPairs: [CurrencyPair] {
    rates.map { $0.pair }
  }

  /// Dynamic flag of the availability of adding currency rates.
  private var isAddingAvailable: Bool {
    CurrencyPair.allPairs.contains { !currencyPairs.contains($0) }
  }

  // MARK: Lifecycle

  init(apiService: CurrencyServiceProtocol, currencyRatesStorageManager: CurrencyRatesStorageManagerProtocol) {
    self.apiService = apiService
    self.currencyRatesStorageManager = currencyRatesStorageManager
  }

  // MARK: Private methods

  private func reloadItems() {
    items.removeAll()

    if isAddingAvailable {
      items.append(.addRate)
    }

    items += rates.compactMap { .rate($0) }
  }

  private func addRate(_ currencyRate: CurrencyRate) {
    currencyRatesStorageManager.addRate(currencyRate)
    reloadData()
    startUpdateLoopIfNeeded()
  }

  private func updateRates(with newRates: [CurrencyRate]) {
    currencyRatesStorageManager.updateRates(with: newRates)
    reloadData()
  }

  private func removeRate(_ currencyRate: CurrencyRate) {
    currencyRatesStorageManager.removeRate(currencyRate)
    reloadData()
    if rates.isEmpty {
      stopUpdateLoop()
    }
  }

  private func requestRatesUpdate() {
    $currentRequestsCount.mutate { $0 += 1 }

    apiService.currencyRates(with: currencyPairs) { [weak self] result in
      guard let self = self else { return }

      self.$currentRequestsCount.mutate { $0 = max(0, $0 - 1) }

      switch result {
      case .success(let ratesInfo):
        let newRates: [CurrencyRate] = ratesInfo.compactMap { rateInfo in
          guard let pair = CurrencyPair(text: rateInfo.key) else { return nil }
          return CurrencyRate(pair: pair, rate: rateInfo.value)
        }
        self.updateRates(with: newRates)
        self.startUpdateLoopIfNeeded()

      case .failure(let error):
        let title = Localization.RatesList.repeatLoading.localized
        let repeatAction = AlertAction(title: title, style: .default) { [weak self] in
          self?.requestRatesUpdate()
        }
        self.view?.showAlert(message: error.localizedDescription, actions: [repeatAction])
      }
    }
  }

  // MARK: Routing

  private func showRemovalAlert(for currencyRate: CurrencyRate) {
    let removeAction = AlertAction(
      title: Localization.Alert.delete.localized,
      style: .destructive
    ) { [weak self] in
      self?.removeRate(currencyRate)
    }
    let cancelAction = AlertAction(title: Localization.Alert.cancel.localized, style: .cancel)
    let actions = [removeAction, cancelAction]

    let currencyPair = currencyRate.pair
    let pairDescription = currencyPair.base.id + "-" + currencyPair.quote.id
    let title = Localization.RatesList.removeRate.localized(withArgs: pairDescription)

    view?.showAlert(title: title, actions: actions, style: .actionSheet)
  }

  private func showAddRateModule() {
    let inputModel = CurrencyPickerInputModel(type: .base, disabledPairs: currencyPairs) { [weak self] pair in
      self?.addRate(CurrencyRate(pair: pair, rate: nil))
    }
    view?.showCurrnecyPicker(with: inputModel)
  }

  // MARK: Timer

  private func startUpdateLoopIfNeeded() {
    guard reloadTimer == nil, !rates.isEmpty else { return }
    DispatchQueue.main.async {
      self.reloadTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] _ in
        guard let self = self else { return }
        if self.currentRequestsCount > 0 || self.rates.isEmpty {
          self.stopUpdateLoop()
        } else {
          self.requestRatesUpdate()
        }
      }
    }
  }

  private func stopUpdateLoop() {
    DispatchQueue.main.async {
      self.reloadTimer?.invalidate()
      self.reloadTimer = nil
    }
  }
}

// MARK: - RatesListPresenterProtocol

extension RatesListPresenter: RatesListPresenterProtocol {

  var numberOfSections: Int {
    items.isEmpty ? 0 : 1
  }

  func reloadData() {
    reloadItems()
    view?.updateView(placeholderHidden: !rates.isEmpty)
  }

  func viewDidAppear() {
    startUpdateLoopIfNeeded()
  }

  func didTapAddRate() {
    showAddRateModule()
  }

  func numberOfItems(in section: Int) -> Int {
    return items.count
  }

  func item(at indexPath: IndexPath) -> RatesListItem? {
    return items[safe: indexPath.row]
  }

  func didSelectItem(at indexPath: IndexPath) {
    guard let selectedItem = item(at: indexPath) else { return }

    switch selectedItem {
    case .addRate:
      showAddRateModule()

    case let .rate(currencyRate):
      showRemovalAlert(for: currencyRate)
    }
  }
}

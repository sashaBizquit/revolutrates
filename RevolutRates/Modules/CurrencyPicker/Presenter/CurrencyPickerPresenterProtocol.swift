//
//  CurrencyPickerPresenterProtocol.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 29.10.2020.
//

import Foundation

/// `RatesList` module `Presenter -> View` interaction protocol.
protocol CurrencyPickerPresenterProtocol: TablePresenting, CurrencyPickerInputProtocol {

  /// Navigation title.
  var navigationTitle: String? { get }

  /// Asks the presenter for a table item to insert its form (cell) in a particular location of the table view.
  ///
  /// - Parameter: Location of the table view.
  /// - Returns: `RatesList` table item.
  func item(at indexPath: IndexPath) -> Currency?
}

//
//  CurrencyRatesStorageProtocol.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 31.10.2020.
//

import Foundation

protocol CurrencyRatesStorageProtocol {
  func data(forKey defaultName: String) -> Data?
  func set(_ value: Any?, forKey defaultName: String)
}

extension UserDefaults: CurrencyRatesStorageProtocol {}

//
//  IconTableViewCell.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 28.10.2020.
//

import UIKit

/// The visual representation of a image+text default row in a table view.
final class IconTableViewCell: UITableViewCell {

  /// Icon image view.
  private let iconImageView = UIImageView()

  /// Title label.
  private let titleLabel = UILabel()

  // MARK: Lifecycle

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)

    setupContentView()
  }

  required init?(coder: NSCoder) {
    assertionFailure("init(coder:) has not been implemented")
    return nil
  }

  // MARK: Private

  private func setupContentView() {
    setupIconImageView()
    setupTitleLabel()
  }

  private func setupIconImageView() {
    contentView.addSubview(iconImageView)

    iconImageView.translatesAutoresizingMaskIntoConstraints = false
    iconImageView.contentMode = .scaleAspectFit

    NSLayoutConstraint.activate(
      [
        iconImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
        iconImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 17),
        iconImageView.heightAnchor.constraint(equalToConstant: 40),
        iconImageView.widthAnchor.constraint(equalToConstant: 40),
        iconImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -11)
      ]
    )
  }

  private func setupTitleLabel() {
    contentView.addSubview(titleLabel)

    titleLabel.translatesAutoresizingMaskIntoConstraints = false
    titleLabel.textColor = Palette.Blue.intense
    titleLabel.textAlignment = .natural
    titleLabel.numberOfLines = 2
    titleLabel.font = UIFont.robotoMedium.withSize(16)

    NSLayoutConstraint.activate(
      [
        titleLabel.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: 17),
        titleLabel.centerYAnchor.constraint(equalTo: iconImageView.centerYAnchor),
        titleLabel.topAnchor.constraint(greaterThanOrEqualTo: contentView.topAnchor, constant: 16),
        titleLabel.rightAnchor.constraint(lessThanOrEqualTo: contentView.rightAnchor, constant: -16)
      ]
    )
  }
}

// MARK: - IconTableViewCellFormItemProtocol

extension IconTableViewCell: FormItemConfigurable {
  func configure(with formItem: IconTableViewCellFormItemProtocol) {
    titleLabel.text = formItem.title
    iconImageView.configure(with: formItem.iconItem)
  }
}

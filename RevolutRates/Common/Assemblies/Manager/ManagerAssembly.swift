//
//  ManagerAssembly.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

/// Protocol for manager assembly.
protocol ManagerAssembly {
  associatedtype Manager
  
  static var manager: Manager { get }
}

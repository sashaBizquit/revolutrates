//
//  ViewAssembly.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 25.10.2020.
//

/// MVP module view assembler protocol.
protocol ViewAssembly {
  associatedtype View: ModuleView
  
  static var view: View { get }
}

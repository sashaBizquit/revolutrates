//
//  AddRateViewFormItem.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 30.10.2020.
//

/// Default `AddRateView` decoration instance.
struct AddRateViewFormItem: AddRateViewFormItemProtocol {
  let topImageViewItem: ImageFormItem
  let middleButtonItem: ButtonFormItem
  let caption: String
}

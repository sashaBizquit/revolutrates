//
//  CurrencyRate.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 25.10.2020.
//

/// Currency rate model.
struct CurrencyRate: Codable, Equatable {

  /// Currency pair.
  let pair: CurrencyPair

  /// Rate value.
  let rate: Double?
}

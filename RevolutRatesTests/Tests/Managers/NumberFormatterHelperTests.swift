//
//  NumberFormatterHelperTests.swift
//  RevolutRatesTests
//
//  Created by Aleksandr Lykov on 31.10.2020.
//

import XCTest
@testable import RevolutRates

/// Tests for numeric-to-string converter.
final class NumberFormatterHelperTests: XCTestCase {

  /// Testing the formation of a currency line from the value of the currency rate.
  func testRateFormatting() {
    let testCases: [Double: String] = [
      0: "0.0000",
      1.0001: "1.0001",
      2.0010: "2.0010",
      3.0100: "3.0100",
      4.1000: "4.1000",
      5: "5.0000",
      60: "60.0000",
      700: "700.0000",
      8000: "8000.0000",
      9.8: "9.8000",
      9.87: "9.8700",
      9.876: "9.8760",
      9.8765: "9.8765",
      98.7654: "98.7654",
      987.6543: "987.6543",
    ]

    testCases.forEach {
      XCTAssertEqual(NumberFormatterHelper.rateString(from: $0.key), $0.value)
    }
  }
}

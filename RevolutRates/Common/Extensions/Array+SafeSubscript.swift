//
//  Array+SafeSubscript.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

extension Array {

  /// Accesses the element at the specified position, if it is there, otherwise - returns `nil`.
  /// 
  /// - Parameter index: The position of the element to access.
  subscript(safe index: Index) -> Element? {
	  guard indices.contains(index) else {
      assertionFailure("Index \(index) out of range \(indices)")
      return nil
    }
	  return self[index]
  }
}

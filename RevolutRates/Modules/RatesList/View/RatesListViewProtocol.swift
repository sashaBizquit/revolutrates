//
//  RatesListViewProtocol.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 26.10.2020.
//

/// `RatesList` module `Presenter -> View` interaction protocol.
protocol RatesListViewProtocol: AlertRouting, CurrencyPickerRouting {

  /// Asks `View` to update all of its content.
  ///
  /// - Parameter placeholderHidden: Placeholder forced display flag
  func updateView(placeholderHidden: Bool)
}

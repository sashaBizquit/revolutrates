//
//  CurrencyRatesStorageManagerStub.swift
//  RevolutRatesTests
//
//  Created by Aleksandr Lykov on 01.11.2020.
//

/// A stub that provides currncy rates managing logic.
final class CurrencyRatesStorageManagerStub {

  // MARK: Properties

  private(set) var currencyRates: [CurrencyRate]

  // MARK: Lifecycle

  init(currencyRates: [CurrencyRate]) {
    self.currencyRates = currencyRates
  }
}

// MARK: - CurrencyRatesStorageManagerProtocol methods

extension CurrencyRatesStorageManagerStub: CurrencyRatesStorageManagerProtocol {

  func addRate(_ currencyRate: CurrencyRate) {
    currencyRates.insert(currencyRate, at: 0)
  }

  func updateRates(with updatedRates: [CurrencyRate]) {
    updatedRates.forEach { rate in
      guard let rateIndex = currencyRates.firstIndex(where: { $0.pair == rate.pair }) else { return }
      currencyRates[rateIndex] = rate
    }
  }

  func removeRate(_ currencyRate: CurrencyRate) {
    currencyRates.removeAll(where: { $0.pair == currencyRate.pair })
  }

  func removeAllRates() {
    currencyRates.removeAll()
  }
}

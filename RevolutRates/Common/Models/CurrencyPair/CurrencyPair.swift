//
//  CurrencyPair.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 26.10.2020.
//

/// Model of the currency pair.
struct CurrencyPair: Equatable, Codable {

  // MARK: Static propertis.
  /// All available currency pairs.
  static let allPairs: [CurrencyPair] = Currency.allCases.flatMap { base in
    Currency.allCases.compactMap { quote in
      guard base != quote else { return nil }
      return CurrencyPair(base: base, quote: quote)
    }
  }

  /// Base currency.
  let base: Currency

  /// Quote currency.
  let quote: Currency

  // MARK: Lifecycle

  init?(base: Currency, quote: Currency) {
    guard base != quote else { return nil }
    self.base = base
    self.quote = quote
  }

  /// The initializer converts the concatenated string of the IDs of the two currencies in a currency pair.
  init?(text: String) {
    let idLength = CommonConstants.currencyIdLength

    // If the length of the concatenated string is exactly equal to twice the length of the IDs
    // and these IDs are processed by the application, we return the currency pair. Otherwise - nil.
    guard text.count == idLength * 2,
          let base = Currency(rawValue: String(text.prefix(idLength)).lowercased()),
          let quote = Currency(rawValue: String(text.suffix(idLength)).lowercased())
    else {
      return nil
    }
    self.init(base: base, quote: quote)
  }
}

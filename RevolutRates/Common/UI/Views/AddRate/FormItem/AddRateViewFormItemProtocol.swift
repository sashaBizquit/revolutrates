//
//  AddRateViewFormItemProtocol.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 30.10.2020.
//

/// `AddRateView` decoration instance protocol.
protocol AddRateViewFormItemProtocol {

  /// Top button form item.
  var topImageViewItem: ImageFormItem { get }

  /// Middle button form item.
  var middleButtonItem: ButtonFormItem { get }

  /// Caption text.
  var caption: String { get }
}

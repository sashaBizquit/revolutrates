//
//  APIServiceManager.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

import Foundation

/// Client-server interaction manager.
final class APIServiceManager: APIServiceManagerProtocol {
  // MARK: Typealiases

  private typealias ResponseProvidingCallback = ResultCallback<(Data?, URLResponse?), APIServiceError.ResponseProviderError>
  private typealias ResponseHandlingResult<T> = Result<T, APIServiceError.ResponseHandlerError>

  /// Logical constants.
  private enum Constants {

    /// The duration of the response from the server.
    static let timeoutInterval: TimeInterval = 5
  }

  // MARK: Properties

  private let session: URLSession = {
    let configuration = URLSessionConfiguration.default
    configuration.timeoutIntervalForRequest = Constants.timeoutInterval
    return URLSession(configuration: configuration)
  }()

  // MARK: Methods

  private func provideRequest(_ urlRequestComponents: URLRequestComponents) -> Result<URLRequest, APIServiceError.RequestProviderError> {
    guard var url = urlRequestComponents.baseURL else  {
      return .failure(.urlNotFound)
    }

    let path = urlRequestComponents.path
    if !path.isEmpty {
      url.appendPathComponent(urlRequestComponents.path)
    }

    let encoding: ParameterEncoder
    switch urlRequestComponents.method {
    default:
      encoding = URLParameterEncoder.default
    }

    var request = URLRequest(url: url)
    request.httpMethod = urlRequestComponents.method.rawValue

    return encoding.encode(request, with: urlRequestComponents.parameters).mapError { .encodingFailed($0) }
  }

  private func provideResponse(with request: URLRequest, completion: @escaping ResponseProvidingCallback) {
    session.dataTask(with: request) { data, response, error in
      if let error = error {
        completion(.failure(.dataTaskFailed(error)))
      } else {
        completion(.success((data, response)))
      }
    }.resume()
  }

  private func handleResponse<T: Decodable>(data: Data?, response: URLResponse?) -> Result<T, APIServiceError.ResponseHandlerError> {
    guard let data = data, !data.isEmpty else {
      return .failure(.emptyResponse)
    }

    do {
      let decodedModel = try JSONDecoder().decode(T.self, from: data)
      return .success(decodedModel)
    } catch {
      return .failure(.decodingFailed(error))
    }
  }
}

// MARK: - RequestPerforming

extension APIServiceManager: RequestPerforming {

  func performRequest<T: Decodable>(urlRequestComponents: URLRequestComponents, completion: @escaping RequestPerformingCallback<T>) {
	  switch provideRequest(urlRequestComponents) {
	  case let .success(request):
  	  provideResponse(with: request) { [weak self] result in
	  	  switch result {
	  	  case let .success((data, response)):
  	  	  guard let self = self else { return assertionFailure() }

  	  	  let handlingResult: ResponseHandlingResult<T> = self.handleResponse(data: data, response: response)
  	  	  completion(handlingResult.mapError { APIServiceError.responseHandlingFailed($0) })

	  	  case let .failure(error):
  	  	  completion(.failure(APIServiceError.responseProvidingFailed(error)))
	  	  }
  	  }

	  case let .failure(error):
  	  completion(.failure(APIServiceError.requestProvidingFailed(error)))
	  }
  }
}

//
//  APIServiceError.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 25.10.2020.
//

/// Error provided by API service.
enum APIServiceError: Error {

  /// Request creation error
  enum RequestProviderError: Error {

    /// Encoding error type.
    enum ParameterEncoderError: Error {

      /// Requst object has no URL.
      case urlNotFound
    }

    /// Encoding error.
    case encodingFailed(ParameterEncoderError)

    /// Requst object has no URL.
    case urlNotFound
  }

  /// Server response receiving error.
  enum ResponseProviderError: Error {

    /// Request execution error.
    case dataTaskFailed(Error)
  }

  /// Request handling error.
  enum ResponseHandlerError: Error {

    /// Response has no data.
    case emptyResponse

    /// Decoding error.
    case decodingFailed(Error)
  }

  /// Request creation error
  case requestProvidingFailed(RequestProviderError)

  /// Server response receiving error.
  case responseProvidingFailed(ResponseProviderError)

  /// Request handling error.
  case responseHandlingFailed(ResponseHandlerError)
}

//
//  AlertRouting.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

import UIKit

/// Alert action UI-independant model.
struct AlertAction {

  /// The text to use for the action button title.
  private(set) var title: String = Localization.Alert.ok.localized.uppercased()

  /// Additional styling information to apply to the button.
  private(set) var style: UIAlertAction.Style = .default

  /// A block to execute when the user selects the action.
  private(set) var handler: VoidCallback?
}

/// `UIAlertController` show protocol.
protocol AlertRouting: ModuleView {

  /// Shows a view controller for displaying an alert to the user.
  ///
  /// - Parameter title: The title of the alert.
  /// - Parameter message: Descriptive text that provides additional details about the reason for the alert.
  /// - Parameter actions: The action object to display as part (button) of the alert.
  /// - Parameter style: The style to use when presenting the alert controller: an action sheet or a modal alert.
  func showAlert(title: String?, message: String?, actions: [AlertAction], style: UIAlertController.Style)
}

extension AlertRouting {
  func showAlert(
    title: String? = nil,
    message: String? = nil,
    actions: [AlertAction] = [AlertAction()],
    style: UIAlertController.Style = .alert
  ) {
	  let localization = Localization.Alert.self
	  let alertTitle = title ?? localization.error.localized
	  let alertViewController = UIAlertController(title: alertTitle, message: message, preferredStyle: style)
    actions.forEach { action in
      alertViewController.addAction(
        UIAlertAction(title: action.title, style: action.style) { _ in action.handler?() }
      )
    }
    DispatchQueue.main.async {
      self.showModule(providedBy: { alertViewController }, type: .present(.default))
    }
  }
}

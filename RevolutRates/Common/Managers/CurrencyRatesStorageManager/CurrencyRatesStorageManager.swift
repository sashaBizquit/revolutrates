//
//  CurrencyRatesStorageManager.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 31.10.2020.
//

import Foundation

final class CurrencyRatesStorageManager {

  private enum Constants {

    /// The key in UserDefaults by which the array of currency pairs is accessed.
    static let storageDataKey = "CurrencyRatesStorageManager_currencyRates"
  }

  @Atomic private(set) var currencyRates: [CurrencyRate] = []

  private let storage: CurrencyRatesStorageProtocol
  private let storageQueue = DispatchQueue(label: "com.lykov.currencyratesstorage", qos: .userInitiated)
  private let encoder = JSONEncoder()
  private let decoder = JSONDecoder()

  // MARK: Lifecycle

  init(storage: CurrencyRatesStorageProtocol) {
    self.storage = storage
  
    currencyRates = loadCurrencyRates()
  }

  // MARK: Storage methods
  /// Returns an array of currency rates located in the storage.
  private func loadCurrencyRates() -> [CurrencyRate] {
    storageQueue.sync {
      guard let data = storage.data(forKey: Constants.storageDataKey),
            let currencyRates = try? decoder.decode([CurrencyRate].self, from: data)
      else {
        return []
      }
      return currencyRates
    }
  }

  /// Puts currency rates into storage.
  private func saveCurrentRates(_ currencyRates: [CurrencyRate]) {
    guard !currencyRates.isEmpty else {
      return storage.set(nil, forKey: Constants.storageDataKey)
    }
    storageQueue.async { [weak self] in
      guard let self = self, let data = try? self.encoder.encode(currencyRates) else {
        return assertionFailure("Failed to save currency rates \(currencyRates)")
      }
      self.storage.set(data, forKey: Constants.storageDataKey)
    }
  }
}

// MARK: - CurrencyRatesStorageManagerProtocol

extension CurrencyRatesStorageManager: CurrencyRatesStorageManagerProtocol {

  func addRate(_ currencyRate: CurrencyRate) {
    // Checking if adding pair was already added.
    guard currencyRates.allSatisfy({ $0.pair != currencyRate.pair }) else {
      return assertionFailure("Adding existing pair attempt")
    }
    $currencyRates.mutate { [weak self] rates in
      rates.insert(currencyRate, at: 0)
      self?.saveCurrentRates(rates)
    }
  }

  func updateRates(with updatedRates: [CurrencyRate]) {
    $currencyRates.mutate { [weak self] rates in
      for index in rates.indices {
        guard let rate = updatedRates.first(where: { $0.pair == rates[index].pair }) else { continue }
        rates[index] = rate
      }
      self?.saveCurrentRates(rates)
    }
  }

  func removeRate(_ currencyRate: CurrencyRate) {
    $currencyRates.mutate { [weak self] rates in
      rates.removeAll { $0.pair == currencyRate.pair }
      self?.saveCurrentRates(rates)
    }
  }

  func removeAllRates() {
    $currencyRates.mutate { [weak self] rates in
      rates.removeAll()
      self?.saveCurrentRates(rates)
    }
  }
}

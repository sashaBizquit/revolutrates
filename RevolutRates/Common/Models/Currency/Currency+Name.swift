//
//  Currency+Name.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 25.10.2020.
//

extension Currency {
  var name: String {
    let localization: Localization.Currency
    switch self {
    case .aud:
      localization = .aud

    case .bgn:
      localization = .bgn

    case .brl:
      localization = .brl

    case .cad:
      localization = .cad

    case .chf:
      localization = .chf

    case .cny:
      localization = .cny

    case .czk:
      localization = .czk

    case .dkk:
      localization = .dkk

    case .eur:
      localization = .eur

    case .gbp:
      localization = .gbp

    case .hkd:
      localization = .hkd

    case .hrk:
      localization = .hrk

    case .huf:
      localization = .huf

    case .idr:
      localization = .idr

    case .ils:
      localization = .ils

    case .inr:
      localization = .inr

    case .isk:
      localization = .isk

    case .jpy:
      localization = .jpy

    case .krw:
      localization = .krw

    case .mxn:
      localization = .mxn

    case .myr:
      localization = .myr

    case .nok:
      localization = .nok

    case .nzd:
      localization = .nzd

    case .php:
      localization = .php

    case .pln:
      localization = .pln

    case .ron:
      localization = .ron

    case .rub:
      localization = .rub

    case .sek:
      localization = .sek

    case .sgd:
      localization = .sgd

    case .thb:
      localization = .thb

    case .usd:
      localization = .usd

    case .zar:
      localization = .zar
    }
    return localization.localized
  }
}

//
//  AppDelegate.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 24.10.2020.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
	  setupWindow()
	  return true
  }

  // MARK: Private

  func setupWindow() {
	  let newWindow = UIWindow(frame: UIScreen.main.bounds)
	  newWindow.rootViewController = UINavigationController(rootViewController: RatesListAssembly.factory())
	  newWindow.makeKeyAndVisible()
	  window = newWindow
  }
}


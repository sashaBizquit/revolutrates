//
//  RequestPerforming.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 25.10.2020.
//

typealias RequestPerformingCallback<T> = ResultCallback<T, APIServiceError>

/// Protocol for executing a request by URL components.
protocol RequestPerforming {

  /// Executes a request by URL components.
  ///
  /// - Parameter urlRequestComponents: URL components.
  /// - Parameter completion: Сlosure called when receiving the result of processing a request.
  func performRequest<T: Decodable>(urlRequestComponents: URLRequestComponents, completion: @escaping RequestPerformingCallback<T>)
}

//
//  ParameterEncoder.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 24.10.2020.
//

import Foundation

/// Request parameters.
typealias Parameters = [String: Any]

/// Request parameters encoder protocol.
protocol ParameterEncoder {
  typealias EncodingResult = Result<URLRequest, APIServiceError.RequestProviderError.ParameterEncoderError>

  /// Generates a request in accordance with the passed parameters.
  ///
  /// - Parameter urlRequest: Processing request.
  /// - Parameter parameters: The parameters that need to be inserted in the request.
  /// - Returns: `EncodingResult` result object.
  func encode(_ urlRequest: URLRequest, with parameters: Parameters?) -> EncodingResult
}

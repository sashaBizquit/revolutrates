//
//  UIImageView+FormItemConfigurable.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

import UIKit

extension UIImageView: FormItemConfigurable {
  func configure(with formItem: ImageFormItem) {
	  switch formItem {
	  case let .iconKey(key):
      guard let icon = UIImage(named: key.rawValue) else {
        return assertionFailure("No image found: \(key.rawValue)")
      }
      image = icon

	  case .empty:
  	  image = nil
	  }
  }
}

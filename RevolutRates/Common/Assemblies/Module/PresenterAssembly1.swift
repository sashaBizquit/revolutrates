//
//  PresenterAssembly.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 07.09.2020.
//

protocol PresenterProtocol: AnyObject {
	associatedtype Router: RouterProtocol

	var router: Router { get }
}

protocol PresenterAssembly {
	associatedtype Presenter: PresenterProtocol

	static var presenter: Presenter { get }
}

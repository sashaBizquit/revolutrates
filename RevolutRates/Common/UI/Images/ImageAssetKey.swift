//
//  ImageAssetKey.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

/// Images located in the Assets.xcassets directory.
enum ImageAssetKey: String {

  // MARK: Icons

  case plus

  // MARK: Currencies

  case aud
  case bgn
  case brl
  case cad
  case chf
  case cny
  case czk
  case dkk
  case eur
  case gbp
  case hkd
  case hrk
  case huf
  case idr
  case ils
  case inr
  case isk
  case jpy
  case krw
  case mxn
  case myr
  case nok
  case nzd
  case php
  case pln
  case ron
  case rub
  case sek
  case sgd
  case thb
  case usd
  case zar
}

//
//  RateTableViewCellFormItemProtocol.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 28.10.2020.
//

/// `RateTableViewCell` decoration instance protocol.
protocol RateTableViewCellFormItemProtocol {

  /// Leading title text.
  var leadingTitle: String { get }

  /// Leading subtitle text.
  var leadingSubtitle: String { get }

  /// Trailing title prefix, e.g. `1.32` for `1.3244` value.
  var trailingTitlePrefix: String { get }

  /// Trailing title suffix, e.g. `44` for `1.3244` value.
  var trailingTitleSuffix: String? { get }

  /// Trailing subtitle text.
  var trailingSubtitle: String { get }
}

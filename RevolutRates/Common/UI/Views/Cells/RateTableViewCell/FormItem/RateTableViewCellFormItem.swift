//
//  RateTableViewCellFormItem.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 28.10.2020.
//

/// Default `RateTableViewCell` decoration instance.
struct RateTableViewCellFormItem: RateTableViewCellFormItemProtocol {
  let leadingTitle: String
  let leadingSubtitle: String
  let trailingTitlePrefix: String
  let trailingTitleSuffix: String?
  let trailingSubtitle: String
}

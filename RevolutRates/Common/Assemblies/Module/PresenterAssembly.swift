//
//  PresenterAssembly.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 25.10.2020.
//

/// MVP module presenter assembler protocol.
protocol PresenterAssembly {
  associatedtype Presenter
  
  static var presenter: Presenter { get }
}

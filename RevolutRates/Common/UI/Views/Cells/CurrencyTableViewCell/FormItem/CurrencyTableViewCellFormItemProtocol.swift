//
//  CurrencyTableViewCellFormItemProtocol.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 30.10.2020.
//

/// `CurrencyTableViewCell` decoration instance protocol.
protocol CurrencyTableViewCellFormItemProtocol {

  /// Icon form item.
  var iconItem: ImageFormItem { get }

  /// Id text representation.
  var id: String { get }

  /// Name text.
  var name: String { get }
}

//
//  Atomic.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 28.10.2020.
//

import Foundation

/// Property wrapper for thread-dependent properties.
@propertyWrapper
final class Atomic<Value> {
  // MARK: Private properties.
  /// The queue on which all operations with the value will be performed.
  private let queue = DispatchQueue(label: "com.lykov.atomic")

  /// Binding variable.
  private var value: Value

  // MARK: Internal properties.
  /// The underlying value referenced by the binding variable.
  var wrappedValue: Value {
    get {
      queue.sync { value }
    }
    set {
      queue.sync { value = newValue }
    }
  }

  /// A projection of the binding value that returns a binding.
  var projectedValue: Atomic<Value> {
    self
  }

  // MARK: Lifecycle

  init(wrappedValue: Value) {
    self.value = wrappedValue
  }

  // MARK: Internal methods
  /// Apply thread-safe mutations to value.
  ///
  /// - Parameter mutation: A closure that implements mutation.
  func mutate(_ mutation: (inout Value) -> Void) {
    return queue.sync { mutation(&value) }
  }
}

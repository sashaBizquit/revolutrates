//
//  CurrencyPickerInputProtocol.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 29.10.2020.
//

/// Selectable currency type.
enum CurrencyPickerType {

  /// Base currency.
  case base

  /// Quote currency with its base pair in assosiated value.
  case quote(base: Currency)
}

/// Module input model used for module configuration.
struct CurrencyPickerInputModel {

  /// Selectable currency type.
  let type: CurrencyPickerType

  /// Currency pairs not available for selection.
  let disabledPairs: [CurrencyPair]

  /// Closure executed when both currencies of a currency pair are selected.
  let callback: Callback<CurrencyPair>?
}

/// `CurrencyPicker` module configuration protocol.
protocol CurrencyPickerInputProtocol {

  /// Configures a module using an input model.
  ///
  /// - Parameter inputModel: Input model.
  func configure(with inputModel: CurrencyPickerInputModel)
}

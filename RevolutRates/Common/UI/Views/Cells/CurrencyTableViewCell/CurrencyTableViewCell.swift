//
//  CurrencyTableViewCell.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 30.10.2020.
//

import UIKit

/// The visual representation of a currency in a table view.
final class CurrencyTableViewCell: UITableViewCell {

  /// View constants.
  private enum Constants {

    /// Icon side size.
    static let iconSide: CGFloat = 24
  }

  // MARK: Properties
  /// Currency flag image view.
  private let iconImageView = UIImageView()

  /// Currency identifier label.
  private let idLabel = UILabel()

  /// Currency name label.
  private let nameLabel = UILabel()

  // MARK: Lifecycle

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupView()
  }

  required init?(coder: NSCoder) {
    assertionFailure("init(coder:) has not been implemented")
    return nil
  }

  // MARK: Private

  private func setupView() {
    setupIconImageView()
    setupIdLabel()
    setupNameLabel()
  }

  private func setupIconImageView() {
    contentView.addSubview(iconImageView)

    iconImageView.translatesAutoresizingMaskIntoConstraints = false
    iconImageView.contentMode = .scaleAspectFill

    let side = Constants.iconSide
    iconImageView.layer.cornerRadius = side / 2
    iconImageView.layer.masksToBounds = true

    NSLayoutConstraint.activate(
      [
        iconImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
        iconImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
        iconImageView.heightAnchor.constraint(equalToConstant: side),
        iconImageView.widthAnchor.constraint(equalToConstant: side),
        iconImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16)
      ]
    )
  }

  private func setupIdLabel() {
    contentView.addSubview(idLabel)

    idLabel.translatesAutoresizingMaskIntoConstraints = false
    idLabel.font = UIFont.robotoRegular.withSize(16)
    idLabel.textColor = Palette.Gray.medium
    idLabel.textAlignment = .natural
    idLabel.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)

    NSLayoutConstraint.activate(
      [
        idLabel.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: 16),
        idLabel.centerYAnchor.constraint(equalTo: iconImageView.centerYAnchor)
      ]
    )
  }

  private func setupNameLabel() {
    contentView.addSubview(nameLabel)

    nameLabel.translatesAutoresizingMaskIntoConstraints = false
    nameLabel.font = UIFont.robotoRegular.withSize(16)
    nameLabel.textColor = Palette.Gray.dark
    nameLabel.textAlignment = .natural

    NSLayoutConstraint.activate(
      [
        nameLabel.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: 65),
        nameLabel.centerYAnchor.constraint(equalTo: iconImageView.centerYAnchor),
        nameLabel.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor, constant: -16),
        nameLabel.trailingAnchor.constraint(greaterThanOrEqualTo: idLabel.trailingAnchor)
      ]
    )
  }
}

// MARK: - FormItemConfigurable

extension CurrencyTableViewCell: FormItemConfigurable {
  func configure(with formItem: CurrencyTableViewCellFormItemProtocol) {
    iconImageView.configure(with: formItem.iconItem)
    idLabel.text = formItem.id
    nameLabel.text = formItem.name
  }
}

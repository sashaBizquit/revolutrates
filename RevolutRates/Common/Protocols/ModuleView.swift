//
//  ModuleView.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 26.10.2020.
//

/// Type of transition between modules.
enum TransitionType {

  /// Type of presentation transition.
  enum PresentationType {

    /// Presents destination view controller natively.
    case `default`

    /// Presents `UINavigationController` with destination view controller in it.
    case inNavigationController
  }

  /// Presentation transition with `PresentationType` type.
  case present(PresentationType)

  /// Push transition.
  case push
}

/// Module View-component protocol.
protocol ModuleView: AnyObject {

  /// Preform transition from current module to other.
  ///
  /// - Parameter factory: Factory that creates the presented module.
  /// - Parameter type: Transition type.
  /// - Parameter isAnimated: Flag indicating whether the transition is animated. Default value is `true`.
  /// - Parameter configure: Closure through which the presented module is configured. Default value is `nil`.
  func showModule<T: ModuleView>(
    providedBy factory: ModuleFactory<T>,
    type: TransitionType,
    animated isAnimated: Bool,
    configure: Callback<T>?
  )

  /// Preform transition from current module to other.
  ///
  /// - Parameter isAnimated: Flag indicating whether the transition is animated.
  /// - Parameter completion: Closure called after the end of transition.
  func close(animated isAnimated: Bool, completion: VoidCallback?)
}

extension ModuleView {
  func showModule<T: ModuleView>(
    providedBy factory: ModuleFactory<T>,
    type: TransitionType,
    animated isAnimated: Bool = true
  ) {
    showModule(providedBy: factory, type: type, animated: isAnimated, configure: nil)
  }

  func close(animated isAnimated: Bool = true) {
    close(animated: isAnimated, completion: nil)
  }
}

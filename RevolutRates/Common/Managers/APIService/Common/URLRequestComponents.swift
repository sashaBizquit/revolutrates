//
//  URLRequestComponents.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

import Foundation

/// HTTP method type.
struct HTTPMethod: RawRepresentable, Equatable, Hashable {

  /// HTTP GET method.
  static let get = HTTPMethod(rawValue: "GET")

  // MARK: RawRepresentable

  let rawValue: String

  init(rawValue: String) {
	  self.rawValue = rawValue
  }
}

/// Components of the request to the server.
protocol URLRequestComponents {

  /// Base URL.
  var baseURL: URL? { get }

  /// HTTP method.
  var method: HTTPMethod { get }

  /// Request path following the base URL.
  var path: String { get }

  /// Request parameters.
  var parameters: Parameters? { get }
}

extension URLRequestComponents {
  var baseURL: URL? {
	  URL(string: CommonConstants.baseURL)
  }

  var method: HTTPMethod {
	  .get
  }

  var path: String {
    ""
  }

  var parameters: Parameters? {
	  nil
  }
}

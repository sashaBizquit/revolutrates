//
//  CurrencyRatesStorageManagerTests.swift
//  RevolutRatesTests
//
//  Created by Aleksandr Lykov on 31.10.2020.
//

import XCTest
@testable import RevolutRates

/// Tests for manager of storing and gaining access to currency rates.
final class CurrencyRatesStorageManagerTests: XCTestCase {

  private var manager: CurrencyRatesStorageManagerProtocol?

  override func setUp() {
    super.setUp()

    manager = CurrencyRatesStorageManager(storage: CurrencyRatesStorageStub())
  }

  override func tearDown() {
    super.tearDown()

    manager = nil
  }

  /// Testing adding a currency pair.
  func testCurrencyRateAdding() {
    guard let manager = manager else {
      return XCTFail("Testable manager wasn't initialized")
    }
    let oldCount = manager.currencyRates.count

    guard let currencyPair = CurrencyPair(base: .aud, quote: .bgn) else {
      return XCTFail("Invalid currency pair")
    }
    let currencyRate = CurrencyRate(pair: currencyPair, rate: 2)
    manager.addRate(currencyRate)

    defer { manager.removeAllRates() }

    let newRates = manager.currencyRates

    guard let storedRate = newRates.first, newRates.count == oldCount + 1 else {
      return XCTFail("Storage manager didn't save only one new currency rate")
    }
    XCTAssertEqual(storedRate, currencyRate, "Currency rate wasn't added to the top")
  }

  /// Testing adding currency pairs.
  func testCurrencyRateUpdating() {
    guard let manager = manager else {
      return XCTFail("Testable manager wasn't initialized")
    }

    let oldCount = manager.currencyRates.count

    guard let firstCurrencyPair = CurrencyPair(base: .aud, quote: .bgn),
          let secondCurrencyPair = CurrencyPair(base: .bgn, quote: .aud)
    else {
      return XCTFail("Invalid currency pair")
    }
    let currencyPairs = [firstCurrencyPair, secondCurrencyPair]
    let currencyRates: [CurrencyRate] = currencyPairs.enumerated().map { index, pair in
      let currencyRate = CurrencyRate(pair: pair, rate: Double(index))
      manager.addRate(currencyRate)
      return currencyRate
    }

    defer {
      manager.removeAllRates()
    }

    let addedRatesCount = currencyRates.count

    guard manager.currencyRates.count == oldCount + addedRatesCount else {
      return XCTFail("Storage manager didn't save only one new currency rate")
    }

    let updatedRates = currencyRates.map { CurrencyRate(pair: $0.pair, rate: ($0.rate ?? 0) + 10) }
    let updatedRatesCount = updatedRates.count

    manager.updateRates(with: updatedRates)

    guard manager.currencyRates.count == oldCount + updatedRatesCount else {
      return XCTFail("Update operation changed rates count")
    }

    for index in updatedRates.indices {
      guard let currencyRate = currencyRates[safe: index],
            let updatedRate = updatedRates[safe: index]
      else {
        return XCTFail("Initial and updated lists have diffrent length")
      }
      XCTAssertEqual(currencyRate.pair, updatedRate.pair, "Update operation changed order")
      XCTAssertNotEqual(currencyRate.rate, updatedRate.rate, "Update operation didn't update rate value")
    }
  }


}

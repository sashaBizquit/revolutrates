//
//  Currency.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 25.10.2020.
//

/// Currencies used in the app.
enum Currency: String, CaseIterable, Codable {

  /// Australian Dollar.
  case aud

  /// Bulgarian Lev.
  case bgn

  /// Brazilian Real.
  case brl

  /// Canadian Dollar.
  case cad

  /// Swiss Franc.
  case chf

  /// Chinese Yuan Renminbi.
  case cny

  /// Czech Koruna.
  case czk

  /// Denmark Krone.
  case dkk

  /// Euro.
  case eur

  /// British Pound.
  case gbp

  /// Hong Kong Dollar.
  case hkd

  /// Croatian Kuna.
  case hrk

  /// Hungarian Forint.
  case huf

  /// Indonesian Rupiah.
  case idr

  /// Israeli Shekel.
  case ils

  /// Indian Rupee.
  case inr

  /// Icelandic Krona.
  case isk

  /// Japanese Yen.
  case jpy

  /// South Korean Won.
  case krw

  /// Mexican Peso.
  case mxn

  /// Malaysian Ringgit.
  case myr

  /// Norwegian Krone.
  case nok

  /// New Zealand Dollar.
  case nzd

  /// Philippine Peso.
  case php

  /// Polish Zloty.
  case pln

  /// Romanian Leu.
  case ron

  /// Russian Ruble.
  case rub

  /// Swedish Krona.
  case sek

  /// Singapore Dollar.
  case sgd

  /// Thai Baht.
  case thb

  /// US Dollar.
  case usd

  /// South African Rand.
  case zar

  /// Currency identifier (abbreviation).
  var id: String {
    rawValue.uppercased()
  }
}

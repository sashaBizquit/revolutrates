//
//  LocalizationProvider.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

import Foundation

/// Protocol that provides localized version of a string.
protocol LocalizationProvider {
  /// Returns the localized version of a string.
  var localized: String { get }

  /// Returns the localized version of a string.
  /// 
  /// - Parameter args: A closure that updates the accumulating
  ///     value with an element of the sequence.
  /// - Returns: Localized version of a string with inserted arguments.
  func localized(withArgs args: CVarArg...) -> String
}

extension LocalizationProvider {

  var localized: String {
	  NSLocalizedString(key, comment: "")
  }

  func localized(withArgs args: CVarArg...) -> String {
    String(format: localized, locale: Locale.current, arguments: args)
  }

  // MARK: Private

  private var key: String {
    "\(type(of: self))_\(self)"
  }
}

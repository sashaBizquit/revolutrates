//
//  URLParameterEncoder.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 24.10.2020.
//

import Foundation

struct URLParameterEncoder {

  // MARK: Properties
  /// `URLParameterEncoder` singleton.
  static let `default` = URLParameterEncoder()

  // MARK: Lifecycle

  private init() {}

  // MARK: Private
  /// Converts parameters into single query-string.
  ///
  /// - Parameter parameters: Parameters instance :)
  /// - Returns: Converted query-string.
  private func query(_ parameters: Parameters) -> String {
	  return parameters
  	  .sorted { $0.key < $1.key }
  	  .flatMap { queryComponents(fromKey: $0, value: $1) }
  	  .joined(separator: "&")
  }

  /// Comverts Parameters elements into query components array.
  ///
  /// - Parameter key: Element's key.
  /// - Parameter value: Element's value.
  /// - Returns: Query components array
  private func queryComponents(fromKey key: Parameters.Key, value: Parameters.Value) -> [String] {
	  if let array = value as? [Any] {
  	  return array.flatMap { queryComponents(fromKey: key, value: $0) }
	  } else {
  	  return ["\(escape(key))=\("\(value)")"]
	  }
  }

  /// Removes illegal characters from the keys of the query.
  ///
  /// - Parameter key: Processing key.
  /// - Returns: Processed key.
  private func escape(_ key: Parameters.Key) -> Parameters.Key {
	  let delimitersToEncode = ":#[]@!$&'()*+,;="
	  let encodableDelimiters = CharacterSet(charactersIn: delimitersToEncode)
	  let allowedCharacters = CharacterSet.urlQueryAllowed.subtracting(encodableDelimiters)

	  return key.addingPercentEncoding(withAllowedCharacters: allowedCharacters) ?? key
  }
}

// MARK: - ParameterEncoder

extension URLParameterEncoder: ParameterEncoder {
  func encode(_ urlRequest: URLRequest, with parameters: Parameters?) -> EncodingResult {
    guard let parameters = parameters else { return .success(urlRequest) }

    var urlRequest = urlRequest

    if urlRequest.httpMethod != nil {
      guard let url = urlRequest.url else {
        return .failure(.urlNotFound)
      }

      if var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false), !parameters.isEmpty {
        let percentEncodedQuery = (urlComponents.percentEncodedQuery.map { $0 + "&" } ?? "") + query(parameters)
        urlComponents.percentEncodedQuery = percentEncodedQuery
        urlRequest.url = urlComponents.url
      }
    } else {
      if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
        urlRequest.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
      }

      urlRequest.httpBody = Data(query(parameters).utf8)
    }

    return .success(urlRequest)
  }
}

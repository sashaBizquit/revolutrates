//
//  ModuleAssembly.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

typealias ModuleFactory<T> = Provider<T>

/// MVP module assembler protocol.
protocol ModuleAssembly: ViewAssembly, PresenterAssembly {
  typealias Factory = ModuleFactory<View>
}

extension ModuleAssembly {

  /// Factory for creating entry point MVP module.
  static var factory: Factory {
    { Self.view }
  }
}

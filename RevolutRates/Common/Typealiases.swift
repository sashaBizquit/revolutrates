//
//  Typealiases.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

typealias VoidCallback = () -> Void
typealias Callback<T> = (T) -> Void
typealias Provider<T> = () -> T
typealias DefaultResult<T> = Result<T, Error>
typealias ResultCallback<T, U: Error> = Callback<Result<T, U>>
typealias DefaultResultCallback<T> = Callback<DefaultResult<T>>
typealias VoidResultCallback = DefaultResultCallback<Void>

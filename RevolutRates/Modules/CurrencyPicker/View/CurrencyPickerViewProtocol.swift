//
//  CurrencyPickerViewProtocol.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 29.10.2020.
//

/// `RatesList` module `Presenter -> View` interaction protocol.
protocol CurrencyPickerViewProtocol: CurrencyPickerRouting { }

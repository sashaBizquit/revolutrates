//
//  IconTableViewCellFormItemProtocol.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 28.10.2020.
//

/// `CurrencyTableViewCell` decoration instance protocol.
protocol IconTableViewCellFormItemProtocol {

  /// Icon form item.
  var iconItem: ImageFormItem { get }

  /// Title text.
  var title: String? { get }
}

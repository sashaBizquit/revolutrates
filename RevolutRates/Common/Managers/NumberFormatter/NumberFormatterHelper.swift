//
//  NumberFormatterHelper.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 29.10.2020.
//

import Foundation

/// Numeric to string converter.
enum NumberFormatterHelper {

  /// Converts the currency rate value to a string.
  ///
  /// - Parameter rate: Currency rate value.
  /// - Returns: Currency rate formatted string.
  static func rateString(from rate: Double) -> String? {
    let numberFormatter = NumberFormatter()
    numberFormatter.minimumIntegerDigits = 1
    numberFormatter.minimumFractionDigits = 4
    numberFormatter.maximumFractionDigits = 4
    numberFormatter.roundingMode = .halfUp
    return numberFormatter.string(from: NSNumber(value: rate))
  }
}

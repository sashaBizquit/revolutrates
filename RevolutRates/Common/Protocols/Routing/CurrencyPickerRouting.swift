//
//  CurrencyPickerRouting.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 29.10.2020.
//

/// `CurrencyPicker` module show protocol.
protocol CurrencyPickerRouting: ModuleView {

  /// Shows the module according to the input model.
  ///
  /// - Parameter inputModel: Input model.
  func showCurrnecyPicker(with inputModel: CurrencyPickerInputModel)
}

extension CurrencyPickerRouting {
  func showCurrnecyPicker(with inputModel: CurrencyPickerInputModel) {
    let transitionType: TransitionType
    switch inputModel.type {
    case .base:
      transitionType = .present(.inNavigationController)

    case .quote:
      transitionType = .push

    }
    showModule(providedBy: CurrencyPickerAssembly.factory, type: transitionType, animated: true) { moduleView in
      moduleView.configure(with: inputModel)
    }
  }
}

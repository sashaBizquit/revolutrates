//
//  RatesListPresenterProtocol.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 06.10.2020.
//

import Foundation

/// `RatesList` table item.
enum RatesListItem {

  /// Currency pair table item.
  case rate(CurrencyRate)

  /// Add rate table item.
  case addRate
}

/// `RatesList` module `View -> Presenter` interaction protocol.
protocol RatesListPresenterProtocol: TablePresenting {

  /// Tells the presenter that the view has been shown.
  func viewDidAppear()

  /// Asks the presenter to update the data.
  func reloadData()

  /// Asks the presenter for a table item to insert its form (cell) in a particular location of the table view.
  ///
  /// - Parameter: Location of the table view.
  /// - Returns: `RatesList` table item.
  func item(at indexPath: IndexPath) -> RatesListItem?

  /// Tells the presenter that rate adding was requested.
  func didTapAddRate()
}

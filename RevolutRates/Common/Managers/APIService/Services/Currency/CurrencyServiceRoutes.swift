//
//  CurrencyService.swift
//  RevolutRates
//
//  Created by Aleksandr Lykov on 20.10.2020.
//

private enum CurrencyServiceRoutes {
  case currencyRates(CurrencyRatesRequestModel)
}

extension CurrencyServiceRoutes: URLRequestComponents {

  var parameters: Parameters? {
	  switch self {
	  case .currencyRates(let requestModel):
      return ["pairs": requestModel.map { $0.base.id + $0.quote.id }]
	  }
  }
}

extension RequestPerforming where Self: CurrencyServiceProtocol {
  func currencyRates(with requestModel: CurrencyRatesRequestModel, completion: @escaping CurrencyRatesRequestCallback) {
	  performRequest(urlRequestComponents: CurrencyServiceRoutes.currencyRates(requestModel), completion: completion)
  }
}
